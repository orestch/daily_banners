// server.js
// load the things we need
var express = require('express'),
		app = express(),
		fs = require('fs-extra'),
		path = require('path');

// set the view engine to jade
app.set('view engine', 'jade');
app.set('view options', {pretty: true});
app.set('views', __dirname + '/views')
// use res.render to load up an ejs view file
if (app.get('env') === 'development') {
  app.locals.pretty = true;
}

var file = path.join(__dirname, 'data/data.json');

var data = fs.readJsonSync(file, {throws: false})


// index page 
app.get('/:id', function(req, res) {
    res.render('pages/index', {title: 'Home', data: data, id: req.params.id});
});

app.listen(8080);
console.log('8080 is the magic port');


